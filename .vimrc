set encoding=utf-8
set fileencoding=utf-8

let mapleader=" "

if has("win32")
  set guifont=Consolas:h11
end

if has("unix")
  set guifont=Fira\ Code\ 10.5
end

filetype off
if has("unix")
  set runtimepath^=~/.fzf
end

set runtimepath^=~/.vim/bundle/Vundle.vim
  call vundle#begin()
    Plugin 'VundleVim/Vundle.vim'

    Plugin 'NLKNguyen/papercolor-theme'

    " Vital
    Plugin 'airblade/vim-rooter'
    Plugin 'tpope/vim-vinegar'

    " Ease
    Plugin 'tpope/vim-commentary'
    Plugin 'godlygeek/tabular'
    Plugin 'lmeijvogel/vim-yaml-helper'

    " Fru fru
    Plugin 'rhysd/git-messenger.vim'

    let g:polyglot_disabled = ['yard']
    Plugin 'sheerun/vim-polyglot'

    Plugin 'ap/vim-css-color'

    if has("unix")
      Plugin 'mileszs/ack.vim'
      Plugin 'junegunn/fzf.vim'
    end

  call vundle#end()
filetype plugin indent on

syntax enable
if has("gui_running")
  set background=light
else
  set background=dark
end
colorscheme PaperColor

" Plugin Configuration
  runtime macros/matchit.vim

  let g:rooter_change_directory_for_non_project_files = 'current'
  let g:rooter_silent_chdir = 1

  let g:vim_yaml_helper#always_get_root = 1
  let g:vim_yaml_helper#auto_display_path = 1

  if has("unix")
    if executable('ag')
      let g:ackprg = 'ag --vimgrep'
      nnoremap gA :Ack!<space>
    endif

    nnoremap <c-p> :Files<cr>
    let g:fzf_preview_window = ''
    let $FZF_DEFAULT_COMMAND='ag --hidden --ignore .git --ignore tmp --ignore node_modules -g ""'
    if has("patch-8.2.0191")
      let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6 } }
    else
      let g:fzf_layout = { 'down': '~30%' }
    endif
  end
" End of Plugin Configuration

"some nice to haves
" Swaps ' and " with each other on the current line
nnoremap \sq :set lz<cr>^f'^f"^V:s/'/!@##@!/g<cr>:s/"/'/g<cr>:s/!@##@!/"/g<cr>:noh<cr>:set nolz<cr>
" beautify json file
nnoremap \j :%!python -m json.tool<cr>:set syntax=json<cr>
" transform erb to haml
nnoremap \h :%! html2haml --erb --ruby19-attributes<cr>:set syntax=haml<cr>
vnoremap \h :!html2haml --erb --ruby19-attributes<cr>:set syntax=haml<cr>
" copy the whole file to clipboard
nnoremap <Leader>ca :0,$y +<cr>
" try a method
nnoremap <Leader>t itry(:<esc>ea)<esc>w
" erb helpers
nnoremap <Leader>< i<% %><esc>F%;a
" Easier saving, never do :W by mistake again
nnoremap <Leader><Leader> :up<cr>
" Stop highlighting only for gui since mapping esc is problematic in terminal
if has("gui_running")
  nnoremap <esc> :noh<cr>
end

" why ditch what's already there?
noremap <Up> gk
noremap <Down> gj

" rails traversal
command! Rdbyml :e config/database.yml

if has("gui_running")
  augroup maximize_gvim
    autocmd!
    if has("unix")
      " i probably have to install this
      " autocmd GUIEnter * call system("wmctrl -i -b add,maximized_vert,maximized_horz -r ".v:windowid)
    end

    if has("win32")
      autocmd GUIEnter * simalt ~x
    end
  augroup end
end

augroup comment_tags
  autocmd!
  autocmd Syntax * syntax match MyCommentTag "@\h\+\|NOTE\|TODO" containedin=.*Comment contained
  autocmd Syntax * syntax cluster rubyNotTop add=MyCommentTag
augroup end

augroup comment_reference_tags
  autocmd!
  autocmd Syntax * syntax match MyCommentReferenceTag ":\h\+" containedin=.*Comment contained
  autocmd Syntax * syntax cluster rubyNotTop add=MyCommentReferenceTag
augroup end

if &background ==# 'dark'
  highlight MyCommentTag gui=bold guifg=#FFD000 ctermfg=220
  highlight MyCommentReferenceTag gui=bold guifg=#444444 guibg=#afff87 ctermfg=238 ctermbg=157
  highlight SpecialKey guifg=#FF0000 ctermfg=196
else
  highlight MyCommentTag gui=bold guifg=#DD0F00 ctermfg=160
  highlight MyCommentReferenceTag gui=bold guifg=#444444 guibg=#afff87 ctermfg=238 ctermbg=157
  highlight SpecialKey guibg=#FAAAFF guifg=#DD0F00 ctermbg=219 ctermfg=160
end
highlight def link rubyTodo MyCommentTag

" set cursorline " Disabled because it's laggy af
set backspace=indent,eol,start
set list listchars=tab:→\ ,trail:·,nbsp:␣
let &showbreak='↳ '
set fillchars=vert:│,fold:─
set smarttab
set expandtab
set tabstop=8
set softtabstop=2
set shiftwidth=2
set history=1000
set display=lastline
set number
set guioptions=ai
set laststatus=2
set wildmenu
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*/node_modules/*
set hlsearch
set autoindent
set incsearch
set ignorecase
set smartcase
set showcmd
set autoread
